package supermercado;

import java.util.Collections;
import java.util.Arrays;
import java.util.LinkedList;

public class Main {

	public static void main(String[] args) 
	{
		LinkedList<Producto> productos = new LinkedList<>();
		productos.addAll(Arrays.asList(
				new Producto[] { new Producto("Coca-Cola Zero", "Litros: 1.5", 20, Producto.UnidadDeVenta.CONTENIDO),
						new Producto("Coca-Cola", "Litros: 1.5", 18, Producto.UnidadDeVenta.CONTENIDO),
						new Producto("Shampoo Sedal", "Contenido: 500ml", 19, Producto.UnidadDeVenta.CONTENIDO),
						new Producto("Frutillas", 64, Producto.UnidadDeVenta.KILO) }));

		productos.forEach(System.out::println);
		System.out.println("=============================");
		Collections.sort(productos);

		System.out.println(String.format("Producto m�s caro: %s", productos.getLast().getNombre()));
		System.out.println(String.format("Producto m�s barato: %s", productos.getFirst().getNombre()));

	}
}
