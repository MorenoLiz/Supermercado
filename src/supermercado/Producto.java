package supermercado;

public class Producto implements Comparable<Producto> 
{
	public static enum UnidadDeVenta 
	{
		KILO, LITRO, CONTENIDO
	}

	private String nombre, descripcion;
	private Integer precio;
	private UnidadDeVenta unidadDeVenta;

	public Producto(String _nombre, String _descripcion, Integer _precio, UnidadDeVenta _unidadDeVenta) 
	{
		checkNull(_nombre, _precio, _unidadDeVenta);
		if (_nombre.isEmpty())
			throw new IllegalArgumentException("El nombre no puede estar vacio");

		this.nombre = _nombre;
		this.descripcion = _descripcion;
		this.precio = _precio;
		this.unidadDeVenta = _unidadDeVenta;
	}

	public Producto(String _nombre, Integer _precio, UnidadDeVenta _unidadDeVenta) 
	{
		this(_nombre, "", _precio, _unidadDeVenta);
	}

	public String getDescripcion() 
	{
		return descripcion;
	}

	public void setDescripcion(String descripcion) 
	{
		this.descripcion = descripcion;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String _nombre) 
	{
		this.nombre = _nombre;
	}

	public Integer getPrecio() 
	{
		return precio;
	}

	public void setPrecio(Integer _precio) 
	{
		this.precio = _precio;
	}

	public UnidadDeVenta getUnidadDeVenta() 
	{
		return unidadDeVenta;
	}

	@Override
	public int compareTo(Producto p2) 
	{
		return getPrecio().compareTo(p2.getPrecio());
	}

	public void checkNull(Object... params) 
	{
		for (Object param : params)
			if (param == null)
				throw new IllegalArgumentException("Ningun argumento puede ser null");
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Nombre: %s /// ", getNombre()));

		if (!getDescripcion().isEmpty())
			sb.append(String.format("%s /// ", getDescripcion()));
		sb.append(String.format("Precio: $%d", getPrecio()));

		if (getUnidadDeVenta() != UnidadDeVenta.CONTENIDO)
			sb.append(String.format(" /// Unidad de venta: %s", getUnidadDeVenta().toString().toLowerCase()));

		return sb.toString();
	}

}
